<?php

namespace Jomarjunior\Day3\Domain;

class DuplicatesService
{
    /**
     * @param Number[] $array
     * @return Number[]
     */
    public static function removeDuplicates(array $array): array
    {
        return array_reduce(
            $array,
            function (array $carry, Number $number) {
                if (count($carry) === 0) {
                    $carry[] = $number;
                    return $carry;
                }

                $duplicates = array_filter(
                    $carry,
                    function (Number $carryNumber) use ($number) {
                        return $carryNumber->equals($number);
                    }
                );

                if (count($duplicates) === 0) {
                    $carry[] = $number;
                }

                return $carry;
            },
            []
        );
    }
}
