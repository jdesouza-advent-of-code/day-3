<?php

namespace Jomarjunior\Day3\Domain;

class NumberSkipperService
{
    public static function skip(Matrix &$matrix, int $x, int $y): Matrix
    {
        if ($x >= $matrix->rowSize()) {
            $x = 0;
            $y++;
        }

        $cell = $matrix->findCell($x, $y);

        if (!$cell->isNumber()) {
            $matrix->movePointerTo($x, $y);
            return $matrix;
        }

        return self::skip($matrix, $x + 1, $y);
    }
}
