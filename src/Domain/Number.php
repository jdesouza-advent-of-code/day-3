<?php

namespace Jomarjunior\Day3\Domain;

class Number
{
    /**
     * @var string
     */
    private $value;
    /**
     * @var Cell[]
     */
    private $digitCells;

    public function __construct(string $value, Cell ...$digitCells)
    {
        if (!ctype_digit($value)) {
            throw new \InvalidArgumentException('Invalid number');
        }

        $this->value = $value;
        $this->digitCells = $digitCells;
    }

    public function addDigitToLeft(Cell $digitCell): self
    {
        $digit = $digitCell->content();

        if (!ctype_digit($digit)) {
            throw new \InvalidArgumentException('Invalid digit');
        }

        return new self($digit . $this->value, $digitCell, ...$this->digitCells);
    }

    public function addDigitToRight(Cell $digitCell): self
    {
        $digit = $digitCell->content();

        if (!ctype_digit($digit)) {
            throw new \InvalidArgumentException('Invalid digit');
        }

        $newDigitCells = $this->digitCells;
        $newDigitCells[] = $digitCell;
        return new self($this->value . $digit, ...$newDigitCells);
    }

    public function __toString(): string
    {
        return ltrim($this->value, '0');
    }

    public function toInteger(): int
    {
        return (int) ltrim($this->value, '0');
    }

    public function equals(self $number): bool
    {
        $equals = true;
        foreach ($this->digitCells as $cell) {
            if (!$number->hasDigit($cell)) {
                $equals = false;
                break;
            }
        }

        return $equals;
    }

    public function hasDigit(Cell $digitCell): bool
    {
        $has = false;
        foreach ($this->digitCells as $cell) {
            if ($cell->equals($digitCell)) {
                $has = true;
                break;
            }
        }

        return $has;
    }

    public function digitCells(): array
    {
        return $this->digitCells;
    }
}
