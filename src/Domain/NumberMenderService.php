<?php

namespace Jomarjunior\Day3\Domain;

class NumberMenderService
{
    public static function mend(
        Matrix $matrix,
        int $x,
        int $y
    ): Number {
        $cell = $matrix->findCell($x, $y);
        if (!$cell->isNumber()) {
            throw new \InvalidArgumentException('Invalid cell');
        }

        $number = new Number($cell->content(), $cell);

        $number = self::mendLeft($matrix, $number, $x, $y);
        $number = self::mendRight($matrix, $number, $x, $y);

        return $number;
    }

    private static function mendLeft(
        Matrix $matrix,
        Number &$number,
        int $x,
        int $y
    ): Number {
        if ($x === 0) {
            return $number;
        }

        $x = $x - 1;
        $cell = $matrix->findCell($x, $y);
        while ($cell->isNumber()) {
            $number = $number->addDigitToLeft($cell);

            $x = $x - 1;

            if ($x < 0) {
                break;
            }

            $cell = $matrix->findCell($x, $y);
        }

        return $number;
    }

    private static function mendRight(
        Matrix $matrix,
        Number &$number,
        int $x,
        int $y
    ): Number {
        if ($x === $matrix->rowSize() - 1) {
            return $number;
        }

        $x = $x + 1;
        $cell = $matrix->findCell($x, $y);
        while ($cell->isNumber()) {
            $number = $number->addDigitToRight($cell);

            $x = $x + 1;

            if ($x >= $matrix->rowSize()) {
                break;
            }

            $cell = $matrix->findCell($x, $y);
        }

        return $number;
    }
}
