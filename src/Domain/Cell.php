<?php

namespace Jomarjunior\Day3\Domain;

class Cell
{
    /**
     * @var string
     */
    private $content;
    /**
     * @var int
     */
    private $x;
    /**
     * @var int
     */
    private $y;

    public function __construct(string $content, int $x, int $y)
    {
        $this->content = $content;
        $this->x = $x;
        $this->y = $y;
    }

    public function content(): string
    {
        return $this->content;
    }

    public function x(): int
    {
        return $this->x;
    }

    public function y(): int
    {
        return $this->y;
    }

    public function equals(Cell $cell): bool
    {
        return $this->x === $cell->x() && $this->y === $cell->y();
    }

    public function isSymbol(): bool
    {
        // Check if the cell is a symbol (not a number or letter) and not a period
        return !ctype_alnum($this->content) && $this->content !== '.';
    }

    public function isAstrix(): bool
    {
        return $this->content === '*';
    }

    public function isEmpty(): bool
    {
        return $this->content === '.';
    }

    public function isNumber(): bool
    {
        return ctype_digit($this->content);
    }

    public function __toString(): string
    {
        return $this->content;
    }
}
