<?php

namespace Jomarjunior\Day3\Domain;

class Matrix
{
    /**
     * @var Cell[][]
     */
    private $cells;

    /**
     * @var int
     */
    private $rowSize;

    /**
     * @var int
     */
    private $xPointer;

    /**
     * @var int
     */
    private $yPointer;

    /**
     * @param Cell[][] $cells
     */
    public function __construct(array $cells)
    {
        $this->xPointer = 0;
        $this->yPointer = 0;

        $this->rowSize = count($cells[0]);

        foreach ($cells as $row) {
            if (count($row) !== $this->rowSize) {
                throw new \InvalidArgumentException('Invalid row size');
            }

            foreach ($row as $cell) {
                if (!$cell instanceof Cell) {
                    throw new \InvalidArgumentException('Invalid cell');
                }
            }
        }

        $this->cells = $cells;
    }

    public static function createEmpty(): self
    {
        return new self([]);
    }

    /**
     * @param Cell[][] $cells
     */
    public static function createFromString(string $string): self
    {
        $rows = explode(PHP_EOL, $string);

        $cells = [];
        $x = 0;
        $y = 0;
        foreach ($rows as $row) {
            $cells[] = array_map(function (string $content) use (&$x, &$y) {
                $x++;
                return new Cell($content, $x - 1, $y);
            }, str_split($row));

            $x = 0;
            $y++;
        }

        return new self($cells);
    }

    public function nextCell(): ?Cell
    {
        $cell = $this->cells[$this->yPointer][$this->xPointer];

        $this->xPointer++;
        if ($this->xPointer >= $this->rowSize) {
            $this->xPointer = 0;
            $this->yPointer++;
        }

        if ($this->yPointer >= count($this->cells)) {
            return null;
        }

        return $cell;
    }

    public function movePointer(int $x, int $y): self
    {
        $this->setXPointer($this->xPointer + $x);
        $this->setYPointer($this->yPointer + $y);

        return $this;
    }

    public function movePointerTo(int $x, int $y): self
    {
        $this->setXPointer($x);
        $this->setYPointer($y);

        return $this;
    }

    public function setXPointer(int $x): self
    {
        if ($x < 0 || $x >= $this->rowSize) {
            throw new \InvalidArgumentException('Invalid pointer');
        }

        $this->xPointer = $x;

        return $this;
    }

    public function xPointer(): int
    {
        return $this->xPointer;
    }

    public function setYPointer(int $y): self
    {
        if ($y < 0 || $y >= count($this->cells)) {
            throw new \InvalidArgumentException('Invalid pointer');
        }

        $this->yPointer = $y;

        return $this;
    }

    public function yPointer(): int
    {
        return $this->yPointer;
    }

    /**
     * @param Cell[][] $cells
     */
    public function addRow(array $row): self
    {
        if (count($row) !== $this->rowSize) {
            throw new \InvalidArgumentException('Invalid row');
        }

        $checkedRow = [];
        foreach ($row as $cell) {
            if (!$cell instanceof Cell) {
                throw new \InvalidArgumentException('Invalid cell');
            }
            $checkedRow[] = $cell;
        }

        $this->cells[] = $checkedRow;

        return $this;
    }

    public function findCell(int $x, int $y): Cell
    {
        if ($x < 0 || $y < 0) {
            throw new \InvalidArgumentException('Invalid cell');
        }

        if ($x >= $this->rowSize) {
            throw new \InvalidArgumentException('Invalid cell');
        }

        if ($y >= count($this->cells)) {
            throw new \InvalidArgumentException('Invalid cell');
        }

        if (!isset($this->cells[$y][$x])) {
            throw new \InvalidArgumentException('Invalid cell');
        }

        return $this->cells[$y][$x];
    }

    public function isCellNeighbourOfSymbol(int $x, int $y): bool
    {
        $neighbours = $this->findNeighboursOfCell($x, $y);
        $symbolNeighbours = array_filter($neighbours, function (Cell $cell) {
            return $cell->isSymbol();
        });

        return count($symbolNeighbours) > 0;
    }

    /**
     * @return Cell[]
     */
    public function findNeighboursOfCell(int $x, int $y): array
    {
        $neighbours = [];

        try {
            $neighbours[] = $this->findCell($x - 1, $y - 1);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x, $y - 1);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x + 1, $y - 1);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x - 1, $y);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x + 1, $y);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x - 1, $y + 1);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x, $y + 1);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x + 1, $y + 1);
        } catch (\InvalidArgumentException $e) {
        }

        return $neighbours;
    }

    /**
     * @return Cell[]
     */
    public function findHorizontalNeighboursOfCell(int $x, int $y): array
    {
        $neighbours = [];

        try {
            $neighbours[] = $this->findCell($x - 1, $y);
        } catch (\InvalidArgumentException $e) {
        }

        try {
            $neighbours[] = $this->findCell($x + 1, $y);
        } catch (\InvalidArgumentException $e) {
        }

        return $neighbours;
    }

    public function rowSize(): int
    {
        return $this->rowSize;
    }

    public function columnSize(): int
    {
        return count($this->cells);
    }

    /**
     * @return string[][]
     */
    public function toArray(): array
    {
        $array = [];
        foreach ($this->cells as $row) {
            $array[] = array_map(function (Cell $cell) {
                return $cell->content();
            }, $row);
        }

        return $array;
    }

    public function __toString(): string
    {
        $string = '';
        foreach ($this->cells as $row) {
            foreach ($row as $cell) {
                $string .= $cell->content();
            }
            $string .= PHP_EOL;
        }

        return $string;
    }
}
