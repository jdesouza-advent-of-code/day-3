<?php

require_once __DIR__ . '/vendor/autoload.php';

use Jomarjunior\Day3\Domain\Cell;
use Jomarjunior\Day3\Domain\DuplicatesService;
use Jomarjunior\Day3\Domain\Matrix;
use Jomarjunior\Day3\Domain\Number;
use Jomarjunior\Day3\Domain\NumberMenderService;
use Jomarjunior\Day3\Domain\NumberSkipperService;

function main(): void {
    $result = 0;
    $resultPart2 = 0;

    // We will read the input from a file
    $input = trim(
        file_get_contents(__DIR__ . '/data/input.txt')
    );

    // We will use a Matrix representation of the input
    $matrix = Matrix::createFromString($input);

    // We will iterate over the matrix, cell by cell, from left to right, top to bottom
    $nextCell = $matrix->nextCell();
    // We will count the total amount of numbers and the total amount of numbers neighbour of symbol
    $totalAmountOfNumbers = 0;
    $totalAmountOfNumbersNeighbourOfSymbol = 0;
    // While there is a next cell ...
    while ($nextCell !== null) {
        // We need to look for numbers
        if ($nextCell->isNumber()) {
            $totalAmountOfNumbers++;
            // Numbers can take more than one cell, so we need to mend them
            $number = NumberMenderService::mend($matrix, $nextCell->x(), $nextCell->y());

            // If any of the digits of the number is a neighbour of a symbol, we need to add the number to the result
            foreach ($number->digitCells() as $digitCell) {
                if ($matrix->isCellNeighbourOfSymbol($digitCell->x(), $digitCell->y())) {
                    $totalAmountOfNumbersNeighbourOfSymbol++;
                    $result = $result + $number->toInteger();
                    // We don't need to check the other digits of the number
                    break;
                }
            }

            // We need to skip the cells that are part of the number because we already checked them
            NumberSkipperService::skip($matrix, $nextCell->x(), $nextCell->y());
        }

        // -------------  Part of the Part 2 of the puzzle -------------
        // We need to look for astrix
        if ($nextCell->isAstrix()) {
            /**
             * @var Cell[] $cellsNeighboursOfAstrix
             */
            $cellsNeighboursOfAstrix = array_values(array_filter(
                $matrix->findNeighboursOfCell($nextCell->x(), $nextCell->y()),
                function ($cell) {
                    return $cell->isNumber();
                }
            ));

            $numbers = array_map(
                function (Cell $cell) use ($matrix) {
                    return NumberMenderService::mend($matrix, $cell->x(), $cell->y());
                },
                $cellsNeighboursOfAstrix
            );

            $duplicatesRemoved = DuplicatesService::removeDuplicates($numbers);

            if (count($duplicatesRemoved) === 2) {
                $resultPart2 = $resultPart2 + array_reduce(
                    $duplicatesRemoved,
                    function (int $carry, $number) {
                        return $carry * $number->toInteger();
                    },
                    1
                );
            }

            if (count($duplicatesRemoved) > 2) {
                throw new \RuntimeException('Somethin went wrong with the neighbours of the astrix');
            }
        }
        // -------------  End of Part of the Part 2 of the puzzle -------------

        // We need to get the next cell
        $nextCell = $matrix->nextCell();
    }

    // We need to print the results
    echo 'Total amount of numbers: ';
    echo $totalAmountOfNumbers . PHP_EOL;

    echo 'Total amount of numbers neighbour of symbol: ';
    echo $totalAmountOfNumbersNeighbourOfSymbol . PHP_EOL;

    // This is the answer to the puzzle
    echo 'Result: ';
    echo $result . PHP_EOL;

    // -------------  Part of the Part 2 of the puzzle -------------
    echo 'Result Part 2: ';
    echo $resultPart2 . PHP_EOL;
    // -------------  End of Part of the Part 2 of the puzzle -------------
}

main();